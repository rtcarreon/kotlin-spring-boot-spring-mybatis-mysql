
CREATE SCHEMA IF NOT EXISTS `testdb`;

USE `testdb`;

-- DROP TABLE IF EXISTS `UserDetails`;
CREATE TABLE `UserDetails` (
  `id` BIGINT(20) AUTO_INCREMENT,
  `birthday` DATE NOT NULL,
  `gender` ENUM('M', 'F', 'X') NOT NULL DEFAULT 'X',
  `full_name` VARCHAR(36) NOT NULL,
  PRIMARY KEY (`id`)
);

-- DROP TABLE IF EXISTS `account`;
CREATE TABLE `Account` (
  `id` BIGINT(20) AUTO_INCREMENT,
  `user_id` BIGINT(20) NOT NULL,
  `username` VARCHAR(36) NOT NULL,
  `active` ENUM('ACTIVE','INACTIVE','INACTIVE_BY_ACCOUNT') NOT NULL DEFAULT 'ACTIVE',
  `date_registered` DATE NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY unq_username (`username`),
  CONSTRAINT FK_UserAccount FOREIGN KEY (`user_id`)
    REFERENCES `UserDetails`(`id`)
);
