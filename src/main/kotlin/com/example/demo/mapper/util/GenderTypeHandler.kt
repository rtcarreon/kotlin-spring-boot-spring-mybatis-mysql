package com.example.demo.mapper.util

import org.apache.ibatis.type.JdbcType
import org.apache.ibatis.type.MappedTypes
import org.apache.ibatis.type.MappedJdbcTypes
import org.apache.ibatis.type.TypeHandler
import java.sql.PreparedStatement
import java.sql.ResultSet
import java.sql.CallableStatement

@MappedTypes(String::class)
@MappedJdbcTypes(JdbcType.VARCHAR)
class GenderTypeHandler : TypeHandler<String> {
	
	override fun setParameter(
			ps: PreparedStatement,
			i: Int,
			parameter: String,
			jdbcType: JdbcType) {
		
        ps.setString(i, if (parameter == "male") "M" else "F")
    }
	
	override fun getResult(rs: ResultSet, columnName: String): String {
        return if (rs.getString(columnName) == "M") "male" else "female"
    }

    override fun getResult(rs: ResultSet, columnIndex: Int): String {
        return if (rs.getString(columnIndex) == "M") "male" else "female"
    }

    override fun getResult(cs: CallableStatement, columnIndex: Int): String {
        return if (cs.getString(columnIndex) == "M") "male" else "female"
    }
}