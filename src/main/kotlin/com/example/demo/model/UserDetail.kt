package com.example.demo.model

import java.util.Date
import com.fasterxml.jackson.annotation.JsonFormat
import com.fasterxml.jackson.annotation.JsonProperty
import com.fasterxml.jackson.annotation.JsonInclude

@JsonInclude(JsonInclude.Include.NON_EMPTY)
data class UserDetail (
	
	@JsonProperty("userId")
	val userId: Long,
	
	@JsonProperty("username")
	val username: String,
	
	@JsonProperty("fullName")
	val fullName: String,
	
	@JsonProperty("gender")
	val gender: String,
	
	@JsonProperty("age")
	val age: Int,
	
	@JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "dd-MM-yyyy", timezone = "UTC")
	@JsonProperty("birthday")
	val birthday: Date,
	
	@JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "dd-MM-yyyy", timezone = "UTC")
	@JsonProperty("dateRegistered")
	val dateRegistered: Date
) {
	// Initializer Block
    init {
        println("Initialized a new UserDetail object with age = $age and username = $username")
    }
}