package com.example.demo.model

class DemoResponse (_message: String, _responseCode: Int) {
	
	// Member Variables (Properties) of the class
	var message: String = _message
	var responseCode: Int = _responseCode

	// Initializer Block
    init {
        println("Initialized a new DemoObject object with message = $message and responseCode = $responseCode")
    }
}