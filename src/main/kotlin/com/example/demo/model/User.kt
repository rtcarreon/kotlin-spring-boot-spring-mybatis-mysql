package com.example.demo.model

import java.util.Date
import com.fasterxml.jackson.annotation.JsonFormat
import com.fasterxml.jackson.annotation.JsonProperty
import com.fasterxml.jackson.annotation.JsonInclude

class User {
	
	var userId: Long = 0L
	
	lateinit var username: String
	
	lateinit var fullName: String
	
	lateinit var gender: String
	
	var age: Int = 0
	
	lateinit var birthday: Date
	
	lateinit var dateRegistered: Date
}