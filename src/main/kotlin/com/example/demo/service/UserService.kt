package com.example.demo

import org.springframework.stereotype.Service
import com.example.demo.dao.UserMapper
import com.example.demo.model.UserDetail
import org.springframework.transaction.annotation.Transactional
import org.springframework.beans.factory.annotation.Autowired
import com.example.demo.model.User

@Transactional
@Service
class UserService {
	
	@Autowired lateinit var userMapper: UserMapper
	
	fun createUser(user: UserDetail): Long {
		var id: Long = 0L
		try {
    			id = userMapper.insert(user)
    		} catch(e:Exception) {
    			//println("${e.message}")
    			throw RuntimeException(e.message)
    		}
		return id
	}
	
	
	fun getByUsername(username: String) : UserDetail {
		var user: UserDetail? = null
		try {
			user = userMapper.getByUsername(username)
		} catch(e:Exception) {
			//println("${e.message}")
			throw RuntimeException(e.message)
		}
		return user
	}
	
	
	fun getUsers() : List<UserDetail> {
		var users: List<UserDetail>? = null
		try {
			users = userMapper.getUsers()
		} catch(e:Exception) {
			//println("${e.message}")
			throw RuntimeException(e.message)
		}
		return users
	}
}