package com.example.demo.controller

import org.springframework.web.bind.annotation.RestController
import org.springframework.web.bind.annotation.RequestMapping

@RestController
class HelloController {
	
	//@RequestMapping("/")
	//fun index(): String {
	//	return "Greetings from Spring Boot!"
	//}
	
	@RequestMapping("/")
	fun index(): String = "Greetings from Spring Boot!"
}