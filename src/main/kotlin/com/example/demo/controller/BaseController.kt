package com.example.demo.controller

import org.springframework.http.ResponseEntity
import com.example.demo.model.DemoResponse
import org.springframework.http.HttpStatus
import org.springframework.beans.factory.annotation.Value

open class BaseController {
	
	@Value("\${demo.token}")
	lateinit var appToken: String
	
	fun valid(token: String?): Boolean {
		return token != null && token == appToken
	}
	
	fun authenticate(token: String?): ResponseEntity<DemoResponse> {
		println("authenticate token :: " + token)
		println("authenticate appToken :: " + appToken)
		var response: ResponseEntity<DemoResponse> = ResponseEntity.created(null)
    			.body(DemoResponse("accepted", HttpStatus.ACCEPTED.value()))
		if (!valid(token)) {
			response = ResponseEntity.badRequest()
        			.body(DemoResponse("Unauthorized", HttpStatus.UNAUTHORIZED.value()))
		}
		
		return response;
	}
}