package com.example.demo.controller

import com.example.demo.model.DemoResponse
import com.example.demo.model.UserDetail
import org.springframework.web.bind.annotation.RestController
import org.springframework.web.bind.annotation.RequestHeader
import org.springframework.web.bind.annotation.RequestBody
import org.springframework.http.ResponseEntity
import org.springframework.http.HttpStatus
import org.springframework.http.MediaType
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RequestMethod
import org.springframework.web.bind.annotation.PostMapping
import org.springframework.beans.factory.annotation.Autowired
import com.example.demo.UserService
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.PathVariable

@RestController
open class UserController : BaseController() {
	
	@Autowired lateinit var userService: UserService
	
	@PostMapping(path = arrayOf("/createUser"),
			consumes = arrayOf(MediaType.APPLICATION_JSON_VALUE),
			produces = arrayOf(MediaType.APPLICATION_JSON_VALUE))
	fun createUser(
			@RequestHeader("Demo-Token") demoToken: String, 
    		@RequestBody user: UserDetail): ResponseEntity<DemoResponse> {
		
		println("createaUser token :: " + demoToken)
		var response: ResponseEntity<DemoResponse> = authenticate(demoToken)
		if (response.body?.responseCode == HttpStatus.ACCEPTED.value()) {
			var id:Long = 0L
    		try {
    			id = userService.createUser(user)
    		} catch(e:Exception) {
    			println("\${e.message}")
    		}
			
			// user not created
	    	if (id == 0L) {
	    		response = ResponseEntity.badRequest()
	        			.body(DemoResponse("bad request", HttpStatus.BAD_REQUEST.value()));
	    		
	    	// user is created
	    	} else {
	    		response = ResponseEntity
	        			.created(null)
	        			.body(DemoResponse("success", HttpStatus.CREATED.value()));
	        }
		}
		
		 return response
	}
	
	@GetMapping(path = arrayOf("/getUser/{username}"),
			produces = arrayOf(MediaType.APPLICATION_JSON_VALUE))
	fun getByUsername(
			@RequestHeader("Demo-Token") demoToken: String, 
    		@PathVariable("username") username: String): ResponseEntity<Any> {
		
		println("get user by username token :: " + demoToken)
		var response: ResponseEntity<Any> = authenticate(demoToken) as ResponseEntity<Any>
		var demoResponse = response.body as DemoResponse
		
		if (demoResponse.responseCode == HttpStatus.ACCEPTED.value()) {
			var userDetail = userService.getByUsername(username)

			// user not created
	    	if (userDetail == null) {
	    		response = ResponseEntity.status(HttpStatus.NOT_FOUND)
	        			.body(DemoResponse("bad request", HttpStatus.BAD_REQUEST.value()));
	    		
	    	// user is created
	    	} else {
	    		response = ResponseEntity.ok()
	        			.body(userDetail);
	        }
		}
		
		 return response
	}
	
	
	@GetMapping(path = arrayOf("/getUsers"),
			produces = arrayOf(MediaType.APPLICATION_JSON_VALUE))
	fun getUsers(
			@RequestHeader("Demo-Token") demoToken: String): ResponseEntity<Any> {
		
		println("getUsers token :: " + demoToken)
		var response: ResponseEntity<Any> = authenticate(demoToken) as ResponseEntity<Any>
		var demoResponse = response.body as DemoResponse
		
		if (demoResponse.responseCode == HttpStatus.ACCEPTED.value()) {
			var users = userService.getUsers()

			// user not created
	    	if (users == null) {
	    		response = ResponseEntity.status(HttpStatus.NOT_FOUND)
	        			.body(DemoResponse("bad request", HttpStatus.BAD_REQUEST.value()));
	    		
	    	// user is created
	    	} else {
	    		response = ResponseEntity.ok()
	        			.body(users);
	        }
		}
		
		 return response
	}
	
}