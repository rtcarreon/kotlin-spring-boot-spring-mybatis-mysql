package com.example.demo.dao

import org.apache.ibatis.annotations.Mapper
import com.example.demo.model.UserDetail
import org.apache.ibatis.annotations.Options
import org.apache.ibatis.annotations.Insert
import org.apache.ibatis.annotations.Delete
import org.apache.ibatis.annotations.Update
import org.apache.ibatis.annotations.Select
import org.apache.ibatis.annotations.Results
import org.apache.ibatis.annotations.Result
import org.apache.ibatis.type.JdbcType
import java.util.Date
import org.apache.ibatis.annotations.SelectProvider
import com.example.demo.mapper.util.GenderTypeHandler
import org.apache.ibatis.annotations.ResultMap
import com.example.demo.model.User
import org.apache.ibatis.annotations.InsertProvider
import com.example.demo.mapper.util.UserDetailSqlBuilder

//@Mapper
interface UserMapper {
	
	//val table: String = "UserDetails"
	
	@Insert("""
			INSERT INTO UserDetails
				(full_name, birthday, gender)
			VALUES (
				#{fullName},
				#{birthday},
				CASE
					WHEN LOWER('$\{gender\}') = 'male'
						THEN 'M'
						ELSE 'F'
				END
			)
	""")
	@Options(useGeneratedKeys = true, keyProperty = "userId", keyColumn = "id")
	//@InsertProvider(type=UserDetailSqlBuilder::class, method="insert")
	fun insert(user: UserDetail): Long
	
	@Results(
		id = "UserDetailResult",
		value = [
	    	Result(property="userId", column="id", id = true),
			Result(property="username", column="username"),
			Result(property="fullName", column="full_name"),
			Result(property="gender", column="gender", typeHandler=GenderTypeHandler::class),
			Result(property="age", column="age"),
			Result(property="birthday", column="birthday", jdbcType=JdbcType.DATE),
			Result(property="dateRegistered", column="date_registered", jdbcType=JdbcType.DATE)
		]
	)
	@Select("""
			SELECT ud.id AS id,
					a.username AS username,
					ud.full_name AS full_name,
					ud.gender AS gender,
					TIMESTAMPDIFF(YEAR, ud.birthday, CURDATE()) AS age,
					ud.birthday AS birthday,
					a.date_registered AS date_registered
			FROM Account a
			INNER JOIN UserDetails ud ON  ud.id = a.user_id
			WHERE a.active = 'ACTIVE' AND a.username = 'john123'
	""")
	//@SelectProvider(type = SqlProviderAdapter::class, method = "select")
	fun getByUsername(username: String): UserDetail
	
	@ResultMap("UserDetailResult")
	@Select("""
			SELECT ud.id AS id,
					a.username AS username,
					ud.full_name AS full_name,
					ud.gender AS gender,
					TIMESTAMPDIFF(YEAR, ud.birthday, CURDATE()) AS age,
					ud.birthday AS birthday,
					a.date_registered AS date_registered
			FROM Account a
			INNER JOIN UserDetails ud ON  ud.id = a.user_id
			WHERE a.active = 'ACTIVE'
	""")
	fun getUsers(): List<UserDetail>
	
	@Update("""
			UPDATE UserDetails
			SET
				full_name = #{fullName},
				birthday = #{birthday},
				gender =
					CASE
						WHEN LOWER('$\{gender\}') = 'male'
						THEN 'M'
						ELSE 'F'
					END
			WHERE id = #{userId}
	""")
	//@UpdateProvider(type = SqlProviderAdapter::class, method = "update")
	fun update(user: UserDetail)
	
	@Delete("""
			DELETE FROM UserDetails WHERE id = #{userId}
	"""")
	//@DeleteProvider(type = SqlProviderAdapter::class, method = "delete")
	fun delete(userId: Long)
	
	
	
}