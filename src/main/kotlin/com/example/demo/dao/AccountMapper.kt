package com.example.demo.dao

import org.apache.ibatis.annotations.Mapper
import org.apache.ibatis.annotations.Insert
import org.apache.ibatis.annotations.Options
import com.example.demo.model.UserDetail

@Mapper
interface AccountMapper {
	
	@Insert("""
			INSERT INTO Account
				(user_id, username, date_registered)
			VALUES (
				#{userId},
				#{username},
				#{dateRegistered}
			)
	""")
	fun insert(account: UserDetail): Long
	
}