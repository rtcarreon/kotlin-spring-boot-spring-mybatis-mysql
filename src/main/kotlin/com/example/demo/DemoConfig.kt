package com.example.demo

import org.springframework.context.annotation.ComponentScan
import org.springframework.context.annotation.Configuration
import org.springframework.context.annotation.PropertySources
import org.springframework.context.annotation.PropertySource
import org.springframework.context.annotation.Bean
import org.springframework.context.support.PropertySourcesPlaceholderConfigurer
import org.springframework.transaction.annotation.EnableTransactionManagement
import org.mybatis.spring.annotation.MapperScan

@ComponentScan(basePackages = arrayOf(
		"com.example.demo",
		"com.example.demo.controller",
		"com.example.demo.controller.async",
		"com.example.demo.dao",
		"com.example.demo.service",
		"com.example.demo.mapper.util"
	))
@Configuration
@PropertySources(value = arrayOf(
		PropertySource("classpath:application.properties"),
		PropertySource("classpath:appConfig.properties")//, ignoreResourceNotFound = true)
))
@EnableTransactionManagement
@MapperScan("com.example.demo.dao")
class DemoConfig {
	
	@Bean open fun propertyConfigInDev(): PropertySourcesPlaceholderConfigurer {
		return PropertySourcesPlaceholderConfigurer()
	}
	
	
}