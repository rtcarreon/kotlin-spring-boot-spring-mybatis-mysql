package com.example.demo.controlller

import org.hamcrest.Matchers.equalTo;
import org.springframework.boot.test.context.SpringBootTest
import org.springframework.http.HttpStatus
import org.junit.jupiter.api.Test
import org.springframework.boot.test.web.client.TestRestTemplate
import com.ninjasquad.springmockk.MockkBean
import io.mockk.every
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.context.properties.EnableConfigurationProperties
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest
import org.springframework.test.web.servlet.MockMvc
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get
import org.springframework.http.MediaType
import org.springframework.test.web.servlet.result.MockMvcResultMatchers.*
import org.springframework.test.context.junit4.SpringRunner
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc
import org.springframework.test.context.ContextConfiguration
import com.example.demo.SpringbootKotlinMybatisApplication
import org.junit.jupiter.api.extension.ExtendWith
import org.springframework.test.context.junit.jupiter.SpringExtension
import com.example.demo.controller.HelloController

//@SpringBootTest
//@AutoConfigureMockMvc
//@ContextConfiguration(classes=SpringbootKotlinMybatisApplication::class)
//@ExtendWith(SpringExtension::class)
@WebMvcTest(HelloController::class)
internal class HelloControllerTest(@Autowired val mockMvc: MockMvc) {
	
	@Test
	fun `Assert hello page` () {
		mockMvc.perform(get("/"))
				.andExpect(status().isOk)
				.andExpect(content().string(equalTo("Greetings from Spring Boot!")))
	}
}